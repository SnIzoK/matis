Rails.application.routes.draw do
  root to: "pages#index"

  controller :pages do
    get "about-us", action: "about_us"
    get "managers-page", action: "managers_page"
    get "manager-one", action: "manager_one"
    get "apartaments-page", action: "apartaments_page"
    get "contacts", action: "contacts"
    get "apartaments-one", action: "apartaments_one"
    get "comparison-page", action: "comparison_page"
  end

  match "*url", to: "application#render_not_found", via: :all
end