class PagesController < ApplicationController

  def index
  end

  def about_us
  end

  def managers_page
  end

  def manager_one
  end

  def apartaments_page
  end

  def contacts
  end

  def apartaments_one    
  end
  
  def comparison_page
  end
  
end