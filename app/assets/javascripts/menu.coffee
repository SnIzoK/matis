$document.on 'click', '.menu-button', ->

  $wrap = $('body')
  $hamburger = $('.hamburger')
  if $wrap.hasClass('opened')
      $wrap.removeClass('opened')
      $hamburger.removeClass('is-active')

  else
      $('body').addClass('opened')
      $hamburger.addClass('is-active')

  $.clickOut('.menu-button',
      ()->
        $('body').removeClass('opened')
        $('.hamburger').removeClass('is-active')
      {except: '.menu-button, baner-burger-menu'}
    )