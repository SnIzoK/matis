
var mapLviv, mapStryi;
var infoWindowLviv, infoWindowStryi;

// markersData variable stores the information necessary to each marker

var markersDataLviv = [
   {
      lat: 49.833888, 
      lng: 24.033760,
      name: "Mattis Company",
      address1:"вул.Саксаганського 24б, м.Львів",
      // address2: "Praia da Barra",
      // postalCode: "3830-772 Gafanha da Nazaré" // don't insert comma in the last item of each marker
   // },
   // {
   //    lat: 40.54955,
   //    lng: -8.7498167,
   //    name: "Camping Costa Velha",
   //    address1:"Quinta dos Patos, n.º 2",
   //    address2: "Praia da Costa Nova",
   //    postalCode: "3830-453 Gafanha da Encarnação" // don't insert comma in the last item of each marker
   // },
   // {
   //    lat: 40.6447167,
   //    lng: -8.7129167,
   //    name: "Camping Gafanha da Boavista",
   //    address1:"Rua dos Balneários do Complexo Desportivo",
   //    address2: "Gafanha da Nazaré",
   //    postalCode: "3830-225 Gafanha da Nazaré" // don't insert comma in the last item of each marker
   } // don't insert comma in the last item
];

var markersDataStryi = [
   {
      lat: 49.256103, 
      lng: 23.846271,
      name: "Mattis Company",
      address1:"вул.Шевченка 24, м.Стрий",
      // address2: "Praia da Barra",
      // postalCode: "3830-772 Gafanha da Nazaré" // don't insert comma in the last item of each marker
   // },
   // {
   //    lat: 40.59955,
   //    lng: -8.7498167,
   //    name: "Camping Costa Nova",
   //    address1:"Quinta dos Patos, n.º 2",
   //    address2: "Praia da Costa Nova",
   //    postalCode: "3830-453 Gafanha da Encarnação" // don't insert comma in the last item of each marker
   // },
   // {
   //    lat: 40.6247167,
   //    lng: -8.7129167,
   //    name: "Camping Gafanha da Nazaré",
   //    address1:"Rua dos Balneários do Complexo Desportivo",
   //    address2: "Gafanha da Nazaré",
   //    postalCode: "3830-225 Gafanha da Nazaré" // don't insert comma in the last item of each marker
   } // don't insert comma in the last item
];

function initialize(setMap) {



   var styles = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "stylers": [
      {
        "color": "#00a5cd"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#00a5cd"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#00a5cd"
      }
    ]
  }
];

   var mapOptions;
   
   if(setMap == "mapLviv") {
       var map_lviv = document.getElementById('map-Lviv')
       if (!map_lviv)
           return;
      mapOptions = {
         center: new google.maps.LatLng(48.033888, 23.003760),
         zoom: 11,
         mapTypeId: 'roadmap',
         styles: styles
      };



      mapLviv = new google.maps.Map(map_lviv, mapOptions);
      
     // a new Info Window is created
     infoWindowLviv = new google.maps.InfoWindow();
  
     // Event that closes the Info Window with a click on the map
     google.maps.event.addListener(mapLviv, 'click', function() {
        infoWindowLviv.close();
    });
      
   } else {
       var map_stryi = document.getElementById('map-Stryi')
       if (!map_stryi)
           return;
      mapOptions = {
         center: new google.maps.LatLng(48.256103, 22.846271),
         zoom: 9,
         mapTypeId: 'roadmap',
         styles: styles
      };
      
     mapStryi = new google.maps.Map(map_stryi, mapOptions);
      
     // a new Info Window is created
     infoWindowStryi = new google.maps.InfoWindow();
  
     // Event that closes the Info Window with a click on the map
     google.maps.event.addListener(mapStryi, 'click', function() {
        infoWindowStryi.close();
    });
   }

   // Finally displayMarkers() function is called to begin the markers creation
   displayMarkers(setMap);

   // Create second map only when initialize function is called for the first time.
   // Second time setMap is equal to mapStryi, so this condition returns false and it will not run  
   if(setMap == "mapLviv"){
      initialize("mapStryi");   
   }
   
}
google.maps.event.addDomListener(window, 'load', function(){ initialize("mapLviv") });


// This function will iterate over markersData array
// creating markers with createMarker function
function displayMarkers(setMap){
   
   var markersData;
   var map;
   
   if(setMap == "mapLviv"){
      markersData = markersDataLviv;
      map = mapLviv;
   } else {
      markersData = markersDataStryi;
      map = mapStryi;
   }
   
   // this variable sets the map bounds according to markers position
   var bounds = new google.maps.LatLngBounds();
  
   // for loop traverses markersData array calling createMarker function for each marker 
   for (var i = 0; i < markersData.length; i++){

      var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
      var name = markersData[i].name;
      var address1 = markersData[i].address1;
      var address2 = markersData[i].address2;
      var postalCode = markersData[i].postalCode;

      createMarker(setMap, latlng, name, address1, address2, postalCode);

      // marker position is added to bounds variable
      bounds.extend(latlng);  
      //bounds.extend({zoom: 11})
   }
   // return;


   // Finally the bounds variable is used to set the map bounds
   // with fitBounds() function
   map.fitBounds(bounds);
}

// This function creates each marker and it sets their Info Window content
function createMarker(setMap, latlng, name, address1, address2, postalCode){
   
   var map;
   var infoWindow;
   
   if(setMap == "mapLviv"){
      map = mapLviv;
      infoWindow = infoWindowLviv;
   } else {
      map = mapStryi;
      infoWindow = infoWindowStryi;
   }
   
   var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      title: name,
      icon: "/assets/img/location.png"
   });

   // This event expects a click on a marker
   // When this event is fired the Info Window content is created
   // and the Info Window is opened.
   google.maps.event.addListener(marker, 'click', function() {
      
      // Creating the content to be inserted in the infowindow
      var iwContent = '<div id="iw_container">' +
            '<div class="iw_title">' + name + '</div>' +
         '<div class="iw_content">' + address1 + '<br />';
          // +
         // address2 + '<br />' +
         // postalCode + '</div></div>';
      
      // including content to the Info Window.
      infoWindow.setContent(iwContent);

      // opening the Info Window in the current map and at the current marker location.
      infoWindow.open(map, marker);
   });
}