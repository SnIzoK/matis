#= require jquery
#= require jquery-ui
#= require jquery_ujs

#= require global

#     P L U G I N S

#= require plugins/jquery-easing
#= require plugins/jquery.appear
#= require plugins/clickout
#= require plugins/datepick
#= require plugins/jquery.validate.min
#= require plugins/form
#= require plugins/jquery.bxslider
#= require plugins/jquery.scrolldelta
#= require plugins/lightgallery.min
# require plugins/scroll-banner
#= require plugins/selectize.min
#= require plugins/parallax.min
#= require plugins/bx
#= require plugins/youTubeEmbed-jquery-1.0
#= require plugins/jquery.nice-select
#= require plugins/jquery.nice-select.min
#= require plugins/nc
# require plugins/jquery-asRange.es
# require plugins/jquery-asRange
#= require plugins/jquery-asRange.min
#= require plugins/range-plugin
#= require plugins/jquery.slider.min
#= require plugins/owl.carousel.min
#= require plugins/owlCarousel

#     I N I T I A L I Z E

#= require google_map
#= require fullpage_banner_height
#= require header
#= require menu
#= require accordion
#= require selectize-initialize
#= require popups
#= require tabs
#= require links
#= require video.js
#= require checkbox