$document.ready ->

  $('.call-us').on 'click', ->

    target_container = $(this).closest(".one-manager")
    target_container.toggleClass("active")

  $.clickOut(".call-popup-box",
    ()->
      target_container = $(this).closest(".one-manager")
      target_container.removeClass("active")

    {except: ".call-us"}
  )


  #    S E R C H    F I L T E R    P O P U P


  $('.search-button ').on 'click', ->

    $('.apartaments-filter-popup ').addClass('visible')
    $('body').addClass('opened-serch')


  #     R E Q U E S T    M A N A G E R 


  $('.request-button ').on 'click', ->

    $('.popup-one-apartament').addClass('visible')
    $('body').addClass('opened-serch')


  #     F I L T E R S    A P A R T A M E N T 


  $('.right-filter-box').on 'click', ->

    $('.filter-mobile').addClass('visible')
    $('body').addClass('opened-serch')

  

  $('.left-filter-box').on 'click', ->

    $('.filter-mobile-price').addClass('visible')
    $('body').addClass('opened-serch')

  
$document.on 'click', ->
  if $('.popup-one-apartament').hasClass('visible')
    $.clickOut('form',
    ()->
      $('.popup-one-apartament').removeClass('visible')
      $('body').removeClass('opened-serch')
    {except: '.request-button, .inner'}
    )
  if $('.apartaments-filter-popup').hasClass('visible')
    $.clickOut('.apartaments-filter-popup',
    ()->
      $('.apartaments-filter-popup').removeClass('visible')
      $('body').removeClass('opened-serch')
    {except: '.search-button, .tabs-box, .filters-content'}
    )
  if $('.filter-mobile').hasClass('visible')
    $.clickOut('.filter-mobile',
    ()->
      $('.filter-mobile').removeClass('visible')
      $('body').removeClass('opened-serch')
    {except: '.left-filter-box, .inner, .accordion-button'}
    )
  if $('.filter-mobile-price').hasClass('visible')
    $.clickOut('.filter-mobile-price',
    ()->
      $('.filter-mobile-price').removeClass('visible')
      $('body').removeClass('opened-serch')
    {except: '.right-filter-box, .inner, .radio-box'}
    )