$(".range-input-m").asRange({
    range: true,
    limit: false,
    max: 250,
    min: 150,
    step: 1,
    // keyboard: true,
    tip: {
        active: 'onMove'
    }
});

$(".range-input-p").asRange({
    range: true,
    limit: false,
    max: 350000,
    min: 75000,
    step: 1,
    // keyboard: true,
    tip: {
        active: 'onMove'
    }
});