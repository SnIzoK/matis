$(document).ready ->
  
  $('.bxslider').bxSlider()

  # about_us_slider = $('.about-us-slider').bxSlider
  #   controls: false
  #   pager: false
  #   speed: 1000
  #   pause: 10000
  #   auto: true
  # $('.about-us-slider-wrap .prev-slide').click ->
  #   about_us_slider.goToPrevSlide()
  # $('.about-us-slider-wrap .next-slide').click ->
  #   about_us_slider.goToNextSlide()


  banner_slider = $('.offers-slider').bxSlider
    controls: false
    pager: false
    speed: 1000
    pause: 5000
    mode: 'fade'
    # auto: true
    onSliderLoad: (currentIndex)->
    onSlideBefore: ($slideElement, oldIndex, newIndex)->
      $('.current-slide').text(banner_slider.getCurrentSlide()+1)
  if banner_slider.getSlideCount
    $('.total-slide').text(banner_slider.getSlideCount())
  $('.actual-offers .prev-slide').click ->
    current = banner_slider.getCurrentSlide()
    banner_slider.goToPrevSlide()
  $('.actual-offers .next-slide').click ->
    current = banner_slider.getCurrentSlide()
    banner_slider.goToNextSlide()



  testimonials_slider = $('.testimonials-slider').bxSlider
    controls: false
    pager: false
    speed: 1000
    mode: 'fade',
    captions: true

  $('.testimonials-wrarp .prev-slide').click ->
    testimonials_slider.goToPrevSlide()
  $('.testimonials-wrarp .next-slide').click ->
    testimonials_slider.goToNextSlide()


  apartament_one_slider = $('.apartament-one-slider').bxSlider
    pagerCustom: '#apartament-pager'
    controls: false
    speed: 1000
    # captions: false